//
//  CardView.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import SwiftUI

struct CardView: View {
    @ObservedObject var viewModel: CardViewModel
    
    let geometryProxy: GeometryProxy
    
    var scale: CGFloat {
        let midPoint: CGFloat = 125

        let viewFrame = geometryProxy.frame(in: CoordinateSpace.global)

        var scale: CGFloat = 1.0
        let deltaXAnimationThreshold: CGFloat = 125

        let diffFromCenter = abs(midPoint - viewFrame.origin.x - deltaXAnimationThreshold / 2)
        if diffFromCenter < deltaXAnimationThreshold {
            scale = 1 + (deltaXAnimationThreshold - diffFromCenter) / 500
        }
        return scale
    }
    
    var body: some View {
        VStack(spacing: 8) {
            AsyncImage(
                path: viewModel.movie.poster_path,
                placeholder: { ProgressView() },
                image: { Image(uiImage: $0).resizable() }
            )
            .scaledToFill()
            .frame(width: 110, height: 180)
            .clipped()
            .cornerRadius(8)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color(white: 0.4))
            )
            .shadow(radius: 3)
            
            Spacer()
            
            Text(viewModel.movie.title ?? "")
                .font(.system(size: 16, weight: .semibold))
                .multilineTextAlignment(.center)
            
            HStack(spacing: 0) {
                ForEach(1..<6) { num in
                    Image(systemName: viewModel.starType(for: num).imageName)
                        .foregroundColor(.orange)
                        .font(.system(size: 14))
                }
            }.padding(.top, -4)
        }
        .scaleEffect(.init(width: scale, height: scale))
        .animation(.easeOut(duration: 1))
        .padding(.vertical)
    }
}

//struct CardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardView()
//    }
//}
