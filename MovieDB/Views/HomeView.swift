//
//  HomeView.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var viewModel: HomeViewModel = HomeViewModel()
    
    @State var selectedMovie: Movie?
    @State var movieListID: MovieAPI.MovieCategories?
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollView(.vertical, showsIndicators: false){
                    VStack {
                        ForEach(viewModel.movieCategories, id: \.self) { category in
                            VStack {
                                HStack {
                                    Text(category.text)
                                        .font(.title2)
                                        .padding()
                                    
                                    Spacer()
                                }
                                
                                MoviesListView(viewModel: .init(movieCategory: category), selectedMovie: $selectedMovie)
                            }
                            .padding(.vertical)
                        }
                    }
                }
            }
            .navigationBarTitle("Filmes")
            .navigationBarItems(trailing: NavigationLink(destination: NavigationLazyView(SearchView())) {
                Image(systemName: "magnifyingglass")
                    .font(.title2)
                    .padding()
            })
            .sheet(item: $selectedMovie) { selectedMovie in
                NavigationLazyView(MovieDetailView(viewModel: .init(movieId: selectedMovie.id)))
            }
        }
    }
}

enum Sheet: String, Identifiable {
    case movieDetail
    
    var id: String {
        rawValue
    }
}


//struct HomeView_Previews: PreviewProvider {
//    static var previews: some View {
//        HomeView()
//    }
//}
