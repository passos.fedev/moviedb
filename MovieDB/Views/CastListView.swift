//
//  CastListView.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import SwiftUI

struct CastListView: View {
    @ObservedObject var viewModel: CastListViewModel
    
    func castItem(for person: CastMember) -> some View {
        HStack {
            AsyncImage(
                path: person.profile_path!,
                placeholder: { ProgressView() },
                image: { Image(uiImage: $0).resizable() }
            )
            .scaledToFill()
            .frame(width: 80, height: 80)
            .clipped()
            .cornerRadius(4)
            
            VStack(alignment: .leading) {
                Text("Nome: \(person.name)")
                    .font(.callout)
                Text("Personagem: \(person.character)")
                    .font(.subheadline)
            }
            
            Spacer()
        }
    }
    
    var body: some View {
        LazyVStack(spacing: 16) {
            ForEach(viewModel.cast) { person in
                if let _ = person.profile_path {
                    castItem(for: person)
                }
            }
        }.padding()
    }
}

//struct CastListView_Previews: PreviewProvider {
//    static var previews: some View {
//        CastListView()
//    }
//}
