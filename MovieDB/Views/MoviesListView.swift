//
//  MoviesList.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import SwiftUI

struct MoviesListView: View {
    @ObservedObject var viewModel: MoviesListViewModel
    @Binding var selectedMovie: Movie?
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack(alignment: .top, spacing: 16) {
                ForEach(viewModel.movies.indices, id: \.self) { index in
                    GeometryReader { proxy in
                        CardView(viewModel: .init(movie: viewModel.movies[index], movieListID: viewModel.movieCategory), geometryProxy: proxy)
                            .onTapGesture {
                                withAnimation(.spring()) {
                                    selectedMovie = viewModel.movies[index]
                                }
                            }
                            .onAppear {
                                if index > viewModel.movies.count - 5, !viewModel.isLoading {
                                    viewModel.send(action: .loadMore)
                                }
                            }
                    }
                    .frame(width: 80, height: 270)
                    .padding(.horizontal, 20)
                    .padding(.vertical, 32)
                }
                Spacer()
                    .frame(width: 16)
            }
        }
    }
}

//struct MoviesListView_Previews: PreviewProvider {
//    static var previews: some View {
//        MoviesListView()
//    }
//}
