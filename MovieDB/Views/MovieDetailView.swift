//
//  MovieDetail.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import SwiftUI
import AVKit

struct MovieDetailView: View {
    @ObservedObject var viewModel: MovieDetailViewModel
    
    var movieInfo: some View {
        LazyVStack(alignment: .leading) {
            HStack {
                HStack(spacing: 0) {
                    ForEach(1..<6) { num in
                        Image(systemName: viewModel.starType(for: num).imageName )
                            .foregroundColor(.orange)
                            .font(.system(size: 14))
                    }
                }
                Text("\(viewModel.movie?.vote_count ?? 0) votos")
                
                Spacer(minLength: 0)
            }
            .padding(.bottom)
            
            Text("Lançamento: \(viewModel.movie?.release_date ?? "")")
                .padding(.bottom, 1)
            Text("Duração: \(viewModel.movie?.runtime ?? 0) minutos")
                .padding(.bottom, 1)
            Text("Status: \(viewModel.movie?.status ?? "")")
                .padding(.bottom, 1)
            Text("Receita: \(viewModel.revenue)")
                .padding(.bottom, 1)
            Text("Gêneros: \(viewModel.genres)")
        }
        .font(.subheadline)
        .padding([.vertical, .leading])
    }
    
    var body: some View {
            if let movie = viewModel.movie {
                ZStack {
                    VStack {
                        AsyncImage(
                            path: movie.backdrop_path ?? "",
                            placeholder: { ProgressView() },
                            image: { Image(uiImage: $0).resizable() },
                            quality: .original
                        )
                        .scaledToFill()
                        .frame(height: 300)
                        .clipped()
                        .cornerRadius(8)
                        
                        Spacer()
                    }
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack {
                            VStack {
                                Spacer()
                                
                                Text(movie.title ?? "")
                                    .font(.title)
                                    .bold()
                                    .padding(.horizontal)
                                    .multilineTextAlignment(.leading)
                            }.frame(height: 230)
                            
                            VStack(alignment: .leading) {
                                HStack {
                                    AsyncImage(
                                        path: movie.poster_path,
                                        placeholder: { ProgressView() },
                                        image: { Image(uiImage: $0).resizable() }
                                    )
                                    .scaledToFill()
                                    .frame(width: 110, height: 180)
                                    .clipped()
                                    .cornerRadius(8)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 8)
                                            .stroke(Color(white: 0.4))
                                    )
                                    .shadow(radius: 3)
                                    
                                    VStack {
                                        Spacer()
                                        movieInfo
                                        Spacer()
                                    }
                                    
                                    Spacer()
                                }
                                .padding()
                                
                                Text("Sinopse")
                                    .font(.title2)
                                    .bold()
                                    .padding(.leading)
                                
                                Text(viewModel.movie?.overview ?? "")
                                    .fixedSize(horizontal: false, vertical: true)
                                    .padding()
                                
                                Text("Elenco")
                                    .font(.title2)
                                    .bold()
                                    .padding(.leading)
                                
                                CastListView(viewModel: .init(movieId: movie.id))
                                
                                Spacer()
                            }
                            .background(Color.background)
                            
                            .clipShape(RoundedRectangle(cornerRadius: 25))
                        }.frame(width: UIScreen.main.bounds.width)
                    }
                }
            }
    }
}

//struct MovieDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        MovieDetailView(viewModel: .init(movieId: 508943))
//    }
//}
