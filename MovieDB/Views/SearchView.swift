//
//  SearchView.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject var viewModel: SearchViewModel = SearchViewModel()
    
    @State var selectedMovie: SearchResult?

    let layout = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    func item(for searchItem: SearchResult) -> some View {
        VStack {
            AsyncImage(
                path: searchItem.poster_path ?? searchItem.profile_path!,
                placeholder: { ProgressView() },
                image: { Image(uiImage: $0).resizable() }
            )
            .scaledToFill()
            .frame(width: 120, height: 180)
            .clipped()
            .cornerRadius(4)
            
            Text(searchItem.name ?? searchItem.title ?? "")
                .padding()
                .padding(.vertical)
        }
        .frame(width: 200, height: 300)
    }
    
    var body: some View {
        VStack {
            TextField("Pesquisar", text: $viewModel.searchTerm)
                .padding(.horizontal, 40)
                .frame(height: 45, alignment: .leading)
                .clipped()
                .cornerRadius(10)
                    .background(RoundedRectangle(cornerRadius: 10).fill(Color.secondary.opacity(0.5)))
                .foregroundColor(.white)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(Color.primary)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 16)
                    }
                )
                .padding()
            
            if viewModel.searchResults.count > 0 {
                ScrollView(showsIndicators: false) {
                    LazyVGrid(columns: layout, spacing: 12) {
                        ForEach(viewModel.searchResults) { result in
                            item(for: result)
                                .onAppear {
                                    if viewModel.shouldLoadMore(after: result) {
                                        viewModel.send(action: .loadMore)
                                    }
                                }
                                .onTapGesture {
                                    if result.media_type == .movie {
                                        withAnimation(.spring()) {
                                            selectedMovie = result
                                        }
                                    }
                                }
                        }
                    }
                }
            } else if !viewModel.isLoading {
                Spacer()
                
                Text("Faça sua pesquisa")
                    .font(.headline)
                
                Spacer()
            } else {
                ProgressView()
            }
        }
        .navigationBarTitle("Encontrar")
        .sheet(item: $selectedMovie) { selectedMovie in
            NavigationLazyView(MovieDetailView(viewModel: .init(movieId: selectedMovie.id)))
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
