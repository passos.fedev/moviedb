//
//  MoviesServices.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation
import Combine

protocol MovieServiceProtocol {
    func list(category: MovieAPI.MovieCategories, page: Int) -> AnyPublisher<ListResponse<Movie>, APIError>
    func details(for movieId: Int) -> AnyPublisher<Movie, APIError>
    func cast(for movieId: Int) -> AnyPublisher<CastResponse, APIError>
    func search(for term: String, page: Int) -> AnyPublisher<SearchResponse, APIError>
}

final class MovieService: MovieServiceProtocol {
    let agent = Agent()
    
    func list(category: MovieAPI.MovieCategories, page: Int) -> AnyPublisher<ListResponse<Movie>, APIError> {
        return agent.run(MovieAPI.getMovies(category, page).urlRequest)
            .mapError { _ in APIError.unknown }
            .map(\.value)
            .eraseToAnyPublisher()
    }
    
    func details(for movieId: Int) -> AnyPublisher<Movie, APIError> {
        return agent.run(MovieAPI.getDetails(movieId).urlRequest)
            .mapError { _ in APIError.unknown }
            .map(\.value)
            .eraseToAnyPublisher()
    }
    
    func cast(for movieId: Int) -> AnyPublisher<CastResponse, APIError> {
        return agent.run(MovieAPI.getCast(movieId).urlRequest)
            .mapError { _ in APIError.unknown }
            .map(\.value)
            .eraseToAnyPublisher()
    }
    
    func search(for term: String, page: Int) -> AnyPublisher<SearchResponse, APIError> {
        return agent.run(MovieAPI.search(term, page).urlRequest)
            .mapError { _ in APIError.unknown }
            .map(\.value)
            .eraseToAnyPublisher()
    }
}
