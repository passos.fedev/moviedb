//
//  MovieEndpoint.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

protocol APIBuilder {
    var apiKey: String { get }
    var urlRequest: URLRequest { get }
    var baseURL: URL { get }
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
}

enum MovieAPI {
    case getMovies(MovieCategories, Int)
    case getDetails(Int)
    case getImage(String, ImageQuality)
    case getCast(Int)
    case search(String, Int)
    
    enum MovieCategories: String, CaseIterable, Hashable {
        case popular
        case top_rated
        case upcoming
        
        var text: String {
            switch self {
            case .popular:
                return "Populares"
            case .top_rated:
                return "Melhores"
            case .upcoming:
                return "Próximos lançamentos"
            }
        }
    }
    
    enum ImageQuality: String {
        case original
        case w500
    }
}

extension MovieAPI: APIBuilder {
    var apiKey: String {
        return "22c9aeff925ad54b2028e292dd0e1fdb"
    }
    
    var urlRequest: URLRequest {
        var components: URLComponents = URLComponents(url: self.baseURL.appendingPathComponent(self.path), resolvingAgainstBaseURL: true)!
        components.queryItems = queryItems
        print(components.url!)
        return URLRequest(url: components.url!)
    }
    
    var baseURL: URL {
        switch self {
        case .getImage(_, let imageQuality):
            return URL(string: "https://image.tmdb.org/t/p/\(imageQuality.rawValue)")!
        default:
            return URL(string: "https://api.themoviedb.org/3")!
        }
    }
    
    var path: String {
        switch self {
        case .getMovies(let category, _):
            return "/movie/\(category)"
        case .getDetails(let movieId):
            return "/movie/\(movieId)"
        case .getImage(let imageId, _):
            return imageId
        case .getCast(let movieId):
            return "/movie/\(movieId)/credits"
        case .search( _, _):
            return "/search/multi"
        }
    }
    
    var queryItems: [URLQueryItem] {
        var queryParams = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "pt-BR")
        ]
        
        switch self {
        case .getMovies( _, let page):
            queryParams.append(URLQueryItem(name: "page", value: String(page)))
            return queryParams
        case .search( let term, let page):
            queryParams.append(contentsOf: [
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "query", value: String(term))
            ])
            return queryParams
        default:
            return queryParams
        }
    }
}
