//
//  ImageCache.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation
import UIKit

protocol ImageCache {
    subscript(_ path: String) -> UIImage? { get set }
}

struct TemporaryImageCache: ImageCache {
    private let cache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        cache.countLimit = 100 // 100 items
        cache.totalCostLimit = 1024 * 1024 * 100 // 100 MB
        return cache
    }()
    
    subscript(_ path: String) -> UIImage? {
        get { cache.object(forKey: path as NSString) }
        set { newValue == nil ? cache.removeObject(forKey: path as NSString) : cache.setObject(newValue!, forKey: path as NSString) }
    }
}
