//
//  Models.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

struct Movie: Codable, Identifiable {
    let id: Int
    let poster_path: String
    let title: String?
    let vote_average: Double
    let vote_count: Int?
    
    let genres: [Genre]?
    let overview: String?
    let original_title: String?
    let revenue: Int?
    let release_date: String?
    let runtime: Int?
    let status: String?
    let backdrop_path: String?
}
