//
//  Search.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import Foundation

struct SearchResponse: Codable {
    let total_pages: Int
    let results: [SearchResult]
}

struct SearchResult: Codable, Identifiable {
    let id: Int
    let poster_path: String?
    let profile_path: String?
    let title: String?
    let name: String?
    let media_type: MediaTypes
    
    var hasPictures: Bool {
        ![poster_path, profile_path].compactMap({ $0 }).isEmpty
    }
}

enum MediaTypes: String, Codable {
    case tv
    case person
    case movie
}
