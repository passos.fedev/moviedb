//
//  Genre.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

struct Genre: Codable {
    let id: Int
    let name: String
}
