//
//  ListResponse.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

struct ListResponse<T>: Codable where T: Codable {
    let page: Int
    let results: [T]
    let total_results: Int
    let total_pages: Int
}
