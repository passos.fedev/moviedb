//
//  Cast.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import Foundation

struct CastResponse: Codable {
    let id: Int
    let cast: [CastMember]
}

struct CastMember: Codable, Identifiable {
    let id: Int
    let character: String
    let name: String
    let profile_path: String?
}
