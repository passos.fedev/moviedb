//
//  Color+Extension.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import SwiftUI

public extension Color {
    static let background = Color(UIColor.systemBackground)
    static let secondaryBackground = Color(UIColor.secondarySystemBackground)
    static let tertiaryBackground = Color(UIColor.tertiarySystemBackground)
}
