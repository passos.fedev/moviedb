//
//  MovieDBApp.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import SwiftUI

@main
struct MovieDBApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
