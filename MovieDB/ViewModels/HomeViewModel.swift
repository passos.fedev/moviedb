//
//  HomeViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

final class HomeViewModel: ObservableObject {
    var movieCategories: [MovieAPI.MovieCategories] {
        return MovieAPI.MovieCategories.allCases
    }
}
