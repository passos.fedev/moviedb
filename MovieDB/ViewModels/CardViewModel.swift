//
//  CardViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation

final class CardViewModel: ObservableObject {
    private(set) var movie: Movie
    private(set) var movieListID: MovieAPI.MovieCategories
    
    init(movie: Movie, movieListID: MovieAPI.MovieCategories) {
        self.movie = movie
        self.movieListID = movieListID
    }
    
    public func starType(for num: Int) -> StarType {
        if Double(num*2) < movie.vote_average {
            return .fullStar
        }
        if ((Double(num*2)-1.9)...Double(num*2)).contains(movie.vote_average) {
            return .halfStar
        }
        return .emptyStar
    }
}

enum StarType {
    case fullStar
    case halfStar
    case emptyStar
    
    var imageName: String {
        switch self {
        case .fullStar:
            return "star.fill"
        case .halfStar:
            return "star.leadinghalf.fill"
        case .emptyStar:
            return "star"
        }
    }
}
