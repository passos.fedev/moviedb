//
//  CastListViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import Foundation
import Combine

final class CastListViewModel: ObservableObject {
    @Published private(set) var cast: [CastMember] = [CastMember]()
    @Published private(set) var error: APIError?
    @Published private(set) var isLoading: Bool = false
    
    private var cancellables: [AnyCancellable] = [AnyCancellable]()
    
    private let moviesService: MovieServiceProtocol
    private let movieId: Int
    
    init(movieId: Int, moviesService: MovieServiceProtocol = MovieService()) {
        self.moviesService = moviesService
        self.movieId = movieId
        
        getCast()
    }
    
    private func getCast() {
        isLoading = true
        self.moviesService.cast(for: movieId)
            .sink { [weak self] completition in
                guard let self = self else { return }
                self.isLoading = false
                switch completition {
                    case let .failure(error):
                        self.error = error
                    case .finished:
                        print("finished")
                }
            } receiveValue: { [weak self] castResponse in
                guard let self = self else { return }
                self.error = nil
                self.cast.append(contentsOf: castResponse.cast)
            }.store(in: &cancellables)
    }
}
