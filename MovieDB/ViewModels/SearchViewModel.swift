//
//  SearchViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 25/06/21.
//

import Foundation
import Combine

final class SearchViewModel: ObservableObject {
    @Published private(set) var searchResults: [SearchResult] = [SearchResult]()
    @Published private(set) var error: APIError?
    @Published private(set) var isLoading: Bool = false
    
    private var cancellables: [AnyCancellable] = [AnyCancellable]()
    
    @Published var searchTerm: String = ""
    private let moviesService: MovieServiceProtocol
    private var page: Int = 1
    
    init(moviesService: MovieServiceProtocol = MovieService()) {
        self.moviesService = moviesService
        
        $searchTerm
            .debounce(for: .milliseconds(800), scheduler: RunLoop.main)
            .removeDuplicates()
            .map({ (string) -> String? in
                self.searchResults = []
                if string.count < 1 {
                    return nil
                }
                self.isLoading = true
                return string
            })
            .compactMap{ $0 }
            .sink { (_) in
            } receiveValue: { [self] (searchField) in
                self.page = 1
                search(for: searchField)
            }.store(in: &cancellables)
        
        search()
    }
    
    enum Action {
        case loadMore
    }
        
    func send(action: Action) {
        switch action {
        case .loadMore:
            self.page += 1
            search()
        }
    }
    
    func shouldLoadMore(after result: SearchResult) -> Bool {
        let index = searchResults.firstIndex(where: { $0.id == result.id })
        if let index = index {
            return index > searchResults.count - 5 && !isLoading
        }
        return false
    }
    
    private func search(for term: String = "") {
        self.moviesService.search(for: searchTerm, page: page)
            .sink { [weak self] completition in
                guard let self = self else { return }
                self.isLoading = false
                switch completition {
                    case let .failure(error):
                        self.error = error
                    case .finished:
                        print("finished")
                }
            } receiveValue: { [weak self] listResponse in
                guard let self = self else { return }
                self.error = nil
                let filteredResult = listResponse.results.filter { $0.hasPictures }
                if self.page > 1 {
                    self.searchResults += filteredResult
                } else {
                    self.searchResults = filteredResult
                }
                
            }.store(in: &cancellables)
    }
}
