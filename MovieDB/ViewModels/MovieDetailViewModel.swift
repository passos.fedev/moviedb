//
//  MovieDetailViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation
import Combine

final class MovieDetailViewModel: ObservableObject {
    @Published private(set) var movie: Movie?
    @Published private(set) var error: APIError?
    @Published private(set) var isLoading: Bool = false
    
    private var cancellables: [AnyCancellable] = [AnyCancellable]()
    
    private let movieId: Int
    private let moviesService: MovieServiceProtocol
    
    init(movieId: Int, moviesService: MovieServiceProtocol = MovieService()) {
        self.movieId = movieId
        self.moviesService = moviesService

        getMovieDetails()
    }
    
    private func getMovieDetails() {
        isLoading = true
        self.moviesService.details(for: movieId)
            .sink { [weak self] completition in
                guard let self = self else { return }
                self.isLoading = false
                switch completition {
                    case let .failure(error):
                        self.error = error
                    case .finished:
                        print("finished")
                }
            } receiveValue: { [weak self] movie in
                guard let self = self else { return }
                self.error = nil
                self.movie = movie
            }.store(in: &cancellables)
    }
    
    public func starType(for num: Int) -> StarType {
        if Double(num*2) < movie?.vote_average ?? 0 {
            return .fullStar
        }
        if ((Double(num*2)-1.9)...Double(num*2)).contains(movie?.vote_average ?? 0) {
            return .halfStar
        }
        return .emptyStar
    }
    
    var genres: String {
        movie?.genres?.map { $0.name }.joined(separator:", ") ?? ""
    }
    
    var revenue: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currencyAccounting
        formatter.locale = Locale(identifier: "pt-BR")
        formatter.currencyCode = "usd"
        return formatter.string(from: NSNumber(value: movie?.revenue ?? 0)) ?? ""
    }
}
