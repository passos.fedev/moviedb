//
//  MoviesListViewModel.swift
//  MovieDB
//
//  Created by Felipe Passos on 24/06/21.
//

import Foundation
import Combine

final class MoviesListViewModel: ObservableObject {
    @Published private(set) var movies: [Movie] = [Movie]()
    @Published private(set) var error: APIError?
    @Published private(set) var isLoading: Bool = false
    
    private var cancellables: [AnyCancellable] = [AnyCancellable]()
    
    private(set) var movieCategory: MovieAPI.MovieCategories
    private let moviesService: MovieServiceProtocol
    private var page: Int = 1
    
    init(movieCategory: MovieAPI.MovieCategories, moviesService: MovieServiceProtocol = MovieService()) {
        self.movieCategory = movieCategory
        self.moviesService = moviesService
        
        getMovies()
    }
    
    enum Action {
        case loadMore
    }
        
    func send(action: Action) {
        switch action {
        case .loadMore:
            self.page += 1
            getMovies()
        }
    }
    
    private func getMovies() {
        isLoading = true
        self.moviesService.list(category: movieCategory, page: page)
            .sink { [weak self] completition in
                guard let self = self else { return }
                self.isLoading = false
                switch completition {
                    case let .failure(error):
                        self.error = error
                    case .finished:
                        print("finished")
                }
            } receiveValue: { [weak self] listResponse in
                guard let self = self else { return }
                self.error = nil
                self.movies.append(contentsOf: listResponse.results)
                
            }.store(in: &cancellables)
    }
}
